package JUEGO_CARTAS_MALO;

/**
 * Un ENUM que guarda los valores de la carta
 * 
 * @author CNR TRABAJO
 *
 */
public enum Valor {
	AS(1), DOS(2), TRES(3), CUATRO(4), CINCO(5), SEIS(6), SIETE(7), OCHO(8), NUEVE(9), DIEZ(10), J(11), REINA(12),
	REY(13);

	private int valornumerico;

	private Valor(int valor) {
		this.valornumerico = valor;
	}

	public int getValorNumerico() {
		return valornumerico;
	}

	@Override
	public String toString() {
		return valornumerico + "";
	}
}