package JUEGO_CARTAS_MALO;

public class Carta {
	private Pal pal;
	private Valor valor;
/**
 * Constructor de la Carta
 * Pal hace referencia al enum Pal y se trata de un tipo de carta
 * Valor hace referencia al enum Valor y se trata del numero de la carta
 * @param pal
 * @param valor
 */
	public Carta(Pal pal, Valor valor) {

		this.pal = pal;
		this.valor = valor;
	}
	/**
	 * Función para pedir y que me devuelva una carta del tipo Palo
	 * @return
	 */

	public Pal getPal() {
		return pal;
	}

	/**
	 * Función para pedir y que me devuelva un valor del tipo Palo
	 * @return
	 */
	public Valor getValor() {
		return valor;
	}

	@Override
	public String toString() {
		return "Carta [pal=" + pal + ", valor=" + valor + "]";
	}

}